package Junit;

import junit.framework.TestCase;
import validation.ValidateCustomer;

public class CreateCustomerTestcase extends TestCase {
	 /*

    Test Number: 1

    Test Objective: empty Customer Attributes

    Test Type: jUnit

    Input(s): Name="",age="",gender="",address="",area="",availability="",amount due=""

    Expected Output: false

    */
	public void testcustomerTest001(){
		ValidateCustomer v_customer = new ValidateCustomer();
		assertEquals(false,v_customer.Checkcustomerdata("", "", "", "", "", "", ""));
	}
	 /*

    Test Number: 2

    Test Objective: Non-empty Valid Customer Attributes

    Test Type: jUnit

    Input(s): Name="Yash Golchha",age="21",gender="M",address="Apart-8 Croi-Ogie"
    ,area="Dublin Road",availability="Available",amount due="0.00"

    Expected Output: true

    */
	public void testcustomerTest002(){
		ValidateCustomer v_customer = new ValidateCustomer();
		assertEquals(true,v_customer.Checkcustomerdata("Yash Golchha", "21", "0.00", "Male", "Available", "Apart-8 Croi-Ogie", "Dublin Road"));
	}

	 /*

    Test Number: 3

    Test Objective: InValid Customer age=17 and amount = -0.01

    Test Type: jUnit

    Input(s): Name="Ya",age="17",gender="M",address="Ap"
    ,area="Du",availability="Available",amount due="-0.01"

    Expected Output: false

    */
	public void testcustomerTest003(){
		ValidateCustomer v_customer = new ValidateCustomer();
		assertEquals(false,v_customer.Checkcustomerdata("Ya", "17", "-0.00", "Male", "Available", "ap", "Du"));
	}
	 /*

    Test Number: 4

    Test Objective: Valid Customer age=18 and amount= 0.00

    Test Type: jUnit

    Input(s): Name="Yas",age="18",gender="Male",address="Apa"
    ,area="Dub",availability="Available",amount due="0.00"

    Expected Output: true

    */
	public void testcustomerTest004(){
		ValidateCustomer v_customer = new ValidateCustomer();
		assertEquals(true,v_customer.Checkcustomerdata("Yas", "18", "-0.00", "Male", "Available", "Apa", "Dub"));
	}
	 /*

    Test Number: 5

    Test Objective: Valid Customer age=120 and amount= 400000.00

    Test Type: jUnit

    Input(s): Name="Yash Yash Yash Yash Yash Yash Yash Yash ",age="120",gender="M",address="Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Croi-Ogie "
    ,area="DublinRoadDublinRoadDublinRoadDublinRoadDublinRoad",availability="Available",amount due="400000.00"

    Expected Output: true

    */
	public void testcustomerTest005(){
		ValidateCustomer v_customer = new ValidateCustomer();
		assertEquals(true,v_customer.Checkcustomerdata("Yash Yash Yash Yash Yash Yash Yash Yash ", "120", "400000.00", "Male", "Available", "Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Croi-Ogie ",
				"DublinRoadDublinRoadDublinRoadDublinRoadDublinRoad"));
	}
	 /*

    Test Number: 6

    Test Objective: InValid Customer age=121 and amount= 400000.01

    Test Type: jUnit

    Input(s): Name="Yash Yash Yash Yash Yash Yash Yash Yash 1",age="120",gender="M",address="Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Croi-Ogie 1"
    ,area="DublinRoadDublinRoadDublinRoadDublinRoadDublinRoad1",availability="Available",amount due="400000.00"

    Expected Output: false

    */
	public void testcustomerTest006(){
		ValidateCustomer v_customer = new ValidateCustomer();
		assertEquals(false,v_customer.Checkcustomerdata("Yash Yash Yash Yash Yash Yash Yash Yash 1", "121", "400000.01", "Male", "Available", "Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Croi-Ogie 1",
				"DublinRoadDublinRoadDublinRoadDublinRoadDublinRoad1"));
	}
	 /*

    Test Number: 7

    Test Objective: Valid Customer age is numbic and amount is decimal

    Test Type: jUnit

    Input(s): Name="Yas",age="18",gender="Male",address="Apa"
    ,area="Dub",availability="Available",amount due="0.00"

    Expected Output: true

    */
	public void testcustomerTest007(){
		ValidateCustomer v_customer = new ValidateCustomer();
		assertEquals(true,v_customer.Checkcustomerdata("Yas", "18", "-0.00", "Male", "Available", "Apa", "Dub"));
	} 
	/*

    Test Number: 8

    Test Objective: InValid Customer age is not numbic and amount is not decimal

    Test Type: jUnit

    Input(s): Name="Yas",age="sa",gender="Male",address="Apa"
    ,area="Dub",availability="Available",amount due="aa"

    Expected Output: true

    */
	public void testcustomerTest008(){
		ValidateCustomer v_customer = new ValidateCustomer();
		assertEquals(false,v_customer.Checkcustomerdata("Yas", "sa", "aa", "Male", "Available", "Apa", "Dub"));
	}
	/*

    Test Number: 9

    Test Objective: Name Contains Char
    
    Test Type: jUnit

    Input(s): Name="Yas",age="sa",gender="Male",address="Apa"
    ,area="Dub",availability="Available",amount due="aa"

    Expected Output: false

    */
	public void testcustomerTest009(){
		ValidateCustomer v_customer = new ValidateCustomer();
		assertEquals(false,v_customer.Checkcustomerdata("Yas12", "18", "2", "Male", "Available", "Apa", "Dub"));
	}
	
}
