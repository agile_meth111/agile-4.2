package Junit;


import junit.framework.Assert;
import junit.framework.TestCase;
import validation.DelPerValidation;

public class DelPerJUnit extends TestCase {
	//Test Number - 1
		//Test Objective - Verify that it must contains name.
		//Input(s) - Ayushi Anusmita
		//Expected Output(s) - No error message
		
		public DelPerValidation objTest = new DelPerValidation();
		/*

	    Test Number: 1

	    Test Objective: empty DeliveryPerson Attributes

	    Test Type: jUnit

	    Input(s): int id = -1500;
			String name="";
			String gender ="";
			String age = "";
			String address= "";
			String area = "";
			String uname = "";
			String pass = "";
	    Expected Output: false

	    */
		public void testAllValid001()
		{
			int id = -1500;
			String name="";
			String gender ="";
			String age = "";
			String address= "";
			String area = "";
			String uname = "";
			String pass = "";
	        boolean expectedResult = false;
	        boolean result = objTest.checkValidLength(id,name, gender, age, address, area, uname, pass);
	        Assert.assertEquals(expectedResult, result);
		}
		/*

	    Test Number: 2

	    Test Objective: Min Invalid DeliveryPerson Attributes

	    Test Type: jUnit

	    Input(s): int id = -1500;
			String name="as";
			String gender ="Male";
			String age = "17";
			String address= "as";
			String area = "as";
			String uname = "as";
			String pass = "Yash6@8";
	    Expected Output: false

	    */
		public void testAllValid002()
		{
			int id = -1500;
			String name="as";
			String gender ="Male";
			String age = "17";
			String address= "as";
			String area = "as";
			String uname = "as";
			String pass = "Yash6@8";
	        boolean expectedResult = false;
	        boolean result = objTest.checkValidLength(id,name, gender, age, address, area, uname, pass);
	        Assert.assertEquals(expectedResult, result);
		}

		/*

	    Test Number: 3

	    Test Objective: Min- Invalid DeliveryPerson Attributes

	    Test Type: jUnit

	    Input(s): int id = 1;
			String name="Yas";
			String gender ="Male";
			String age = "18";
			String address= "add";
			String area = "das";
			String uname = "Asd";
			String pass = "Yash6@8S";
	    Expected Output: true

	    */
		public void testAllValid003()
		{
			int id = 1;
			String name="Yas";
			String gender ="Male";
			String age = "18";
			String address= "add";
			String area = "das";
			String uname = "Asd";
			String pass = "Yash6@8S";
	        boolean expectedResult = true;
	        boolean result = objTest.checkValidLength(id,name, gender, age, address, area, uname, pass);
	        Assert.assertEquals(expectedResult, result);
		}


		/*

	    Test Number: 4

	    Test Objective: Max- valid DeliveryPerson Attributes

	    Test Type: jUnit

	    Input(s): int id = 2,147,483,647;
			String name="Yash Yash Yash Yash Yash Yash Yash Yash ";
			String gender ="Male";
			String age = "18";
			String address= "Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie ";
			String area = "DublinRoadDublinRoadDublinRoadDublinRoadDublinRoad";
			String uname = "Asd12Asd12Asd12Asd12";
			String pass = "Yash6@8SYash6@8SYash6@8S1";
	    Expected Output: true

	    */
		public void testAllValid004()
		{
			int id = 2147483647;
			String name="Yash Yash Yash Yash Yash Yash Yash Yash ";
			String gender ="Male";
			String age = "120";
			String address= "Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie ";
			String area = "DublinRoadDublinRoadDublinRoadDublinRoadDublinRoad";
			String uname = "Asd12Asd12Asd12Asd12";
			String pass = "Yash6@8SYash6@8SYash6@8S1";
	        boolean expectedResult = true;
	        boolean result = objTest.checkValidLength(id,name, gender, age, address, area, uname, pass);
	        Assert.assertEquals(expectedResult, result);
		}

		/*

	    Test Number: 5

	    Test Objective: Max+ valid DeliveryPerson Attributes

	    Test Type: jUnit

	    Input(s): int id = 2,147,483,647;
			String name="Yash Yash Yash Yash Yash Yash Yash Yash ";
			String gender ="Male";
			String age = "121";
			String address= "Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie ";
			String area = "DublinRoadDublinRoadDublinRoadDublinRoadDublinRoad";
			String uname = "Asd12Asd12Asd12Asd12";
			String pass = "Yash6@8SYash6@8SYash6@8S1";
	    Expected Output: true

	    */
		public void testAllValid005()
		{
			int id = 2147483647;
			String name="Yash Yash Yash Yash Yash Yash Yash Yash 1";
			String gender ="Male";
			String age = "121";
			String address= "AApart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie Apart- 18 Croi-Ogie ";
			String area = "DDublinRoadDublinRoadDublinRoadDublinRoadDublinRoad";
			String uname = "AAsd12Asd12Asd12Asd12";
			String pass = "aYash6@8SYash6@8SYash6@8S1";
	        boolean expectedResult = false;
	        boolean result = objTest.checkValidLength(id,name, gender, age, address, area, uname, pass);
	        Assert.assertEquals(expectedResult, result);
		}
		/*

	    Test Number: 6

	    Test Objective:Age is Numbric
	    
	    Test Type: jUnit

	    Input(s): int id = 1;
			String name="Yas";
			String gender ="Male";
			String age = "18";
			String address= "add";
			String area = "das";
			String uname = "Asd";
			String pass = "Yash6@8S";
	    Expected Output: true

	    */
		public void testAllValid006()
		{
			int id = 1;
			String name="Yas";
			String gender ="Male";
			String age = "18";
			String address= "add";
			String area = "das";
			String uname = "Asd";
			String pass = "Yash6@8S";
	        boolean expectedResult = true;
	        boolean result = objTest.checkValidLength(id,name, gender, age, address, area, uname, pass);
	        Assert.assertEquals(expectedResult, result);
		}


		/*

	    Test Number: 7

	    Test Objective:Age is not Numbric
	    
	    Test Type: jUnit

	    Input(s): int id = 1;
			String name="Yas";
			String gender ="Male";
			String age = "18a";
			String address= "add";
			String area = "das";
			String uname = "Asd";
			String pass = "Yash6@8S";
	    Expected Output: true

	    */
		public void testAllValid007()
		{
			int id = 1;
			String name="Yas";
			String gender ="Male";
			String age = "18a";
			String address= "add";
			String area = "das";
			String uname = "Asd";
			String pass = "Yash6@8S";
	        boolean expectedResult = false;
	        boolean result = objTest.checkValidLength(id,name, gender, age, address, area, uname, pass);
	        Assert.assertEquals(expectedResult, result);
		}

		/*

	    Test Number: 8

	    Test Objective: Valid UserName Valid Password
	    
	    Test Type: jUnit

	    Input(s): int id = 1;
			String name="Yas";
			String gender ="Male";
			String age = "18a";
			String address= "add";
			String area = "das";
			String uname = "Asd";
			String pass = "Yash6@8S";
	    Expected Output: true

	    */
		public void testAllValid008()
		{
			int id = 1;
			String name="Yas";
			String gender ="Male";
			String age = "18";
			String address= "add";
			String area = "das";
			String uname = "Asd";
			String pass = "Yash6@8S";
	        boolean expectedResult = true;
	        boolean result = objTest.checkValidLength(id,name, gender, age, address, area, uname, pass);
	        Assert.assertEquals(expectedResult, result);
		}
		/*

	    Test Number: 9

	    Test Objective: In-Valid UserName(With Special Char) In-Valid Password(Without Special Char)
	    
	    Test Type: jUnit

	    Input(s): int id = 1;
			String name="Yas";
			String gender ="Male";
			String age = "18a";
			String address= "add";
			String area = "das";
			String uname = "Asd";
			String pass = "Yash6@8S";
	    Expected Output: false

	    */
		public void testAllValid010()
		{
			int id = 1;
			String name="Yas";
			String gender ="Male";
			String age = "18";
			String address= "add";
			String area = "das";
			String uname = "Asd@";
			String pass = "Yashs1asS";
	        boolean expectedResult = false;
	        boolean result = objTest.checkValidLength(id,name, gender, age, address, area, uname, pass);
	        Assert.assertEquals(expectedResult, result);
		}
		
		/*

	    Test Number: 10

	    Test Objective: In-Valid UserName In-Valid Password(Without Number)
	    
	    Test Type: jUnit

	    Input(s): int id = 1;
			String name="Yas";
			String gender ="Male";
			String age = "18a";
			String address= "add";
			String area = "das";
			String uname = "Asd";
			String pass = "Yash6@8S";
	    Expected Output: true

	    */
		public void testAllValid009()
		{
			int id = 1;
			String name="Yas";
			String gender ="Male";
			String age = "18";
			String address= "add";
			String area = "das";
			String uname = "Asd";
			String pass = "Yash@sS";
	        boolean expectedResult = false;
	        boolean result = objTest.checkValidLength(id,name, gender, age, address, area, uname, pass);
	        Assert.assertEquals(expectedResult, result);
		}
		/*

	    Test Number: 10

	    Test Objective: In-Valid Password(Without Capital Letter)
	    
	    Test Type: jUnit

	    Input(s): int id = 1;
			String name="Yas";
			String gender ="Male";
			String age = "18a";
			String address= "add";
			String area = "das";
			String uname = "Asd";
			String pass = "Yash6@8S";
	    Expected Output: true

	    */
		public void testAllValid011()
		{
			int id = 1;
			String name="Yas";
			String gender ="Male";
			String age = "18";
			String address= "add";
			String area = "das";
			String uname = "Asd";
			String pass = "yash1@ss";
	        boolean expectedResult = false;
	        boolean result = objTest.checkValidLength(id,name, gender, age, address, area, uname, pass);
	        Assert.assertEquals(expectedResult, result);
		}
		/*

	    Test Number: 12

	    Test Objective:In-Valid Password(Without Small Char)
	    
	    Test Type: jUnit

	    Input(s): int id = 1;
			String name="Yas";
			String gender ="Male";
			String age = "18a";
			String address= "add";
			String area = "das";
			String uname = "Asd";
			String pass = "Yash6@8S";
	    Expected Output: true

	    */
		public void testAllValid012()
		{
			int id = 1;
			String name="Yas";
			String gender ="Male";
			String age = "18";
			String address= "add";
			String area = "das";
			String uname = "Asd";
			String pass = "YASH2@12";
	        boolean expectedResult = false;
	        boolean result = objTest.checkValidLength(id,name, gender, age, address, area, uname, pass);
	        Assert.assertEquals(expectedResult, result);
		}

		/*

	    Test Number: 13

	    Test Objective: Valid Password
	    
	    Test Type: jUnit

	    Input(s): int id = 1;
			String name="Yas";
			String gender ="Male";
			String age = "18a";
			String address= "add";
			String area = "das";
			String uname = "Asd";
			String pass = "Yash6@8S";
	    Expected Output: true

	    */
		public void testAllValid013()
		{
			int id = 1;
			String name="Yas";
			String gender ="Male";
			String age = "18";
			String address= "add";
			String area = "das";
			String uname = "Asd";
			String pass = "Yash2@12";
	        boolean expectedResult = true;
	        boolean result = objTest.checkValidLength(id,name, gender, age, address, area, uname, pass);
	        Assert.assertEquals(expectedResult, result);
		}
		/*

	    Test Number: 14

	    Test Objective: InValid Password
	    
	    Test Type: jUnit

	    Input(s): int id = 1;
			String name="Yas21";
			String gender ="Male";
			String age = "18a";
			String address= "add";
			String area = "das";
			String uname = "Asd";
			String pass = "Yash6@8S";
	    Expected Output: true

	    */
		public void testAllValid014()
		{
			int id = 1;
			String name="Yas21";
			String gender ="Male";
			String age = "18";
			String address= "add";
			String area = "das";
			String uname = "Asd";
			String pass = "Yash2@12";
	        boolean expectedResult = false;
	        boolean result = objTest.checkValidLength(id,name, gender, age, address, area, uname, pass);
	        Assert.assertEquals(expectedResult, result);
		}

		
		
		
		
		
		
/*
		public void testAllValid006()
		{
			int id = 11;
			String name="Ayushi Anusmita";
			String gender ="female";
			int age = 25;
			String address= "Apt 24, Croi Oige, Athlone";
			String area = "Athlone";
			String uname = "Ayushi Anusmita";
			String pass = "917822";
	        boolean expectedResult = true;
	        boolean result = objTest.checkValidLength(id,name, gender, age+"", address, area, uname, pass);
	        Assert.assertEquals(expectedResult, result);
		}

		public void testNovalues002()
		{
			int id = 0;
			String name="";
			String gender ="";
			int age = 0;
			String address= "";
			String area = "";
			String uname = "";
			String pass = "";
	        boolean expectedResult = false;
	        boolean result = objTest.checkValidLength(id, name, gender, age+"", address, area, uname, pass);
	        Assert.assertEquals(expectedResult, result);
		}
		public void testAllInvalid003() {
	    	int id = 12;
			String name="Ay";
			String gender ="";
			int age = 14;
			String address= "Ap";
			String area = "At";
			String uname = "Ay";
			String pass = "91";
	        boolean expectedResult = false;
	        boolean result = objTest.checkValidLength(id,name,gender,age+"",address,area,uname,pass);
	        Assert.assertEquals(expectedResult, result);
	    }

	    public void testAllOutLength004() {
	    	int id = 12;
			String name="AyushiAyushiAyushiAyushiAyushiAyushi";
			String gender ="";
			int age = 121;
			String address= "AyushiAyushiAyushiAyushiAyushiAyushiAyushiAyushiAyushi";
			String area = "At";
			String uname = "AyushiAyushiAyushiAyushi";
			String pass = "91AyushiAyushi12Ayushi";
	        boolean expectedResult = false;
	        boolean result = objTest.checkValidLength(id, name,gender,age+"",address,area,uname,pass);
	        Assert.assertEquals(expectedResult, result);
	    }
*/
	    
	
}
