import java.awt.Color;
import java.util.List;
import java.util.ListIterator;

import javax.swing.JLabel;
import javax.swing.JTextField;

public class ValidationImp1 {

	public boolean isNULL(List<JTextField> j,JLabel err) {
		// TODO Auto-generated method stub
		ListIterator<JTextField> listItr = j.listIterator();
		 while(listItr.hasNext()) {
			 JTextField str1=listItr.next();
	         if( str1.getForeground().equals(Color.GRAY) ){
	        	 err.setText("*"+str1.getText()+" is Empty");
	        	 return true;
	         }
		 }
		 return false;
		
	}
	public void cleardata(List<JTextField> asList) {
		// TODO Auto-generated method stub
		ListIterator<JTextField> listItr = asList.listIterator();
		 while(listItr.hasNext()) {
			 JTextField str1=listItr.next();
			 str1.setText("");
			 str1.requestFocus();
			 str1.requestFocus(false);
		 }
		 
	}

	public boolean isNULL1(List<JTextField> asList, JLabel err) {
		// TODO Auto-generated method stub
		ListIterator<JTextField> listItr = asList.listIterator();
		 while(listItr.hasNext()) {
			 JTextField str1=listItr.next();
	         if( str1.getText().equals("") ){
	        	 err.setText("*"+str1.getText()+" is Empty");
	        	 return true;
	         }
		 }
		return false;
	}
}
