package services;

import java.sql.SQLException;
import java.util.ArrayList;

import Data.CustomerData;
import Data.DelPerData;
import impl.DelPerImpl;
import validation.DelPerValidation;

public class DelPerService {

	public String createDelPerson(int text1, String text2, String text3, String text4,
			String text5, String text6, String text7, String text8) {
		// TODO Auto-generated method stub
		//add the Logic..............
		DelPerValidation v =new DelPerValidation();
		Boolean result = v.checkValidLength(text1, text2, text3, text4, text5, text6, text7, text8);
		String message;
		if (result == true){
			int id =text1;	
			String name = text2;
			String gender=text3;
			int age = Integer.parseInt(text4);
			String address= text5;
			String area = text6;
			String uname = text7;
			String pass = text8;
			
			DelPerData customer1 =new DelPerData(id,name,gender,age,address,area,uname,pass);
			DelPerImpl i = new DelPerImpl();
			//i.update(id);
			message=i.adddelper(customer1);
		}
		else
		{return "Enter the Valid Field";
		}
		return message;
	}
	public String updateCustomer(int text1, String text2, String text3, String text4,
			String text5, String text6, String text7, String text8) throws Exception {
		// TODO Auto-generated method stub
		//add the Logic..............
		DelPerValidation v =new DelPerValidation();
		Boolean result = v.checkValidLength(text1, text2, text3, text4, text5, text6, text7, text8);
		String message="";
		if (result == true){
		int id =text1;	
		String name = text2;
		String gender=text3;
		int age = Integer.parseInt(text4);
		String address= text5;
		String area = text6;
		String uname = text7;
		String pass = text8;
		
		DelPerData customer1 =new DelPerData(id,name,gender,age,address,area,uname,pass);
		DelPerImpl i = new DelPerImpl();
		i.update(customer1);
		//i.adddelper(customer1);
		message=i.update(customer1);
		}
		else
		{return "Enter the Valid Field";
		}
		return message;
	}
	public String deleteDeliveryPerson(int id) {
		DelPerImpl i = new DelPerImpl();
		try {
			return i.detele(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}
		return "Error in Deleting";
	}
public ArrayList<DelPerData> viewDeliveryPerson(){
		
		return new DelPerImpl().viewDeliveryPerson();
		
	}
}

