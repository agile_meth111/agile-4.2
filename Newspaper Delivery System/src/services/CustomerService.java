package services;

import java.util.ArrayList;

import Data.CustomerData;
import impl.CustomerImpl;
import validation.ValidateCustomer;

public class CustomerService {
	private String message="";
	private final CustomerImpl custimpl = new CustomerImpl();
	public String createCustomer(String text2, String text3, String text4, String string, String string2,
			String text5, String text6) {
		// TODO Auto-generated method stub
		//add the Logic..............
		ValidateCustomer v =new ValidateCustomer();
		message=v.Checkcustomerdata1(text2, text3, text4, string, string2, text5, text6);
		if (message == "Success"){
			
		String name=text2;
		int age =Integer.parseInt(text3);
		double amountdue = Double.parseDouble(text4);
		char gender= string.charAt(0);
		int avail = (string2 == "Available") ? 1 : 0 ;
		String address = text5;
		String area = text6;
		
		CustomerData customer1 =new CustomerData(0,name,age,amountdue,gender,avail,address,area);
		message=custimpl.createProduct(customer1);
		
		if( message=="Inserted")
			return "Insert Successful";
		else
			return message;
		}
		return message;
	}
	public ArrayList<CustomerData> viewCustomer(){
		
		return custimpl.viewCustomer();
		
	}
	public String deletCustomer(int id){
		return custimpl.deleteCustomer(id);
	}
	public String updateCustomer(int id,String text2, String text3, String text4, String string, String string2,
			String text5, String text6) {
		// TODO Auto-generated method stub
		//add the Logic..............
		ValidateCustomer v =new ValidateCustomer();
		message=v.Checkcustomerdata1(text2, text3, text4, string, string2, text5, text6);
		if (message == "Success"){
			String name=text2;
			int age =Integer.parseInt(text3);
			double amountdue = Double.parseDouble(text4);
			char gender= string.charAt(0);
			int avail = (string2 == "Available") ? 1 : 0 ;
			String address = text5;
			String area = text6;
			System.out.print(id);
			CustomerData customer1 =new CustomerData(id,name,age,amountdue,gender,avail,address,area);
			message=custimpl.updateCustomer(customer1);
			if( message=="Updated")
				return "Updated Successful";
			else
				return message;
			}
			return message;
		
	}
}
