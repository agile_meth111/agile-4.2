package validation;

import java.util.regex.Pattern;

public class ValidateCustomer {
	public String name;
	public String age ;
	public String amountdue;
	public String gender;
	public String avail ;
	public String address;
	public String area;
	@Override
	public String toString() {
		return "ValidateCustomer [name=" + name + ", age=" + age + ", amountdue=" + amountdue + ", gender=" + gender
				+ ", avail=" + avail + ", address=" + address + ", area=" + area + "]";
	}

	public boolean Checkcustomerdata(String name1, String text3, String text4, String string, String string2,
			String text5, String text6){

		name=name1;
		age =text3;
		amountdue = text4;
		gender= string;
		avail = string2;
		address = text5;
		area = text6;
		System.out.println(toString());
		System.out.println(name.length()+" "+address.length()+" "+area.length());
		
		boolean result =true;
		if(name =="" || age =="" || amountdue =="" || gender =="" || avail =="" || address =="" || area ==""){
			System.out.println("error empty");
			return false;
		}else if(name.length()<3 || name.length()>40 || 
				address.length()<3|| address.length()>250 ||
				area.length()<3||area.length()>50)
		{
			System.out.println("error String length");
			System.out.println(name.length()+" "+address.length()+" "+area.length());
			return false;
		}else if (!Pattern.matches("[a-zA-Z ]+",name)) {
			System.out.print(name);
			return false;	
		}else if(!isNumeric(age) || !isDouble(amountdue))
		{
			System.out.print("error is numbric");
			return false;
		}else if(Integer.parseInt(age)<18 || Integer.parseInt(age)>120 || Double.parseDouble(amountdue)<0|| Double.parseDouble(amountdue)>400000)
		{
			System.out.print("error val");
			return false;
		}else{
			return true;
		}
	}
	public String Checkcustomerdata1(String name1, String text3, String text4, String string, String string2,
			String text5, String text6){

		name=name1;
		age =text3;
		amountdue = text4;
		gender= string;
		avail = string2;
		address = text5;
		area = text6;
		//System.out.println(toString());
		//System.out.println(name.length()+" "+address.length()+" "+area.length());
		
		String result ="";
		if(name =="")
			return result="*Name feild should not be empty";
		else if(age =="")
			return result="*Age feild should not be empty";
		else if(amountdue =="")
			return result="*Amount due feild should not be empty";
		else if(gender =="" )
			return result="*Gender is not valid";
		else if( avail =="" )
			return result="*Availability is not valid";
		else if( address =="" )
			return result="*Address feild should not be empty";
		else if( area =="")
			return result="*Area feild should not be empty";
		else if(name.length()<3 )
			return result="*Name feild Length is less than 3";
		else if(name.length()>40)
			return result="*Name feild Length is greater than 40";
		else if(address.length()<3)
			return result="*Address feild Length is less than 3";
		else if( address.length()>250 )
			return result="*Address feild Length is greater than 250";
		else if(area.length()<3 )
			return result="*Area feild Length is less than 3";
		else if(area.length()>50)
			return result="*Area feild Length is greater than 50";
		else if (!Pattern.matches("[a-zA-Z ]+",name))
			return result="*Name Should only contain letters";	
		else if(!isNumeric(age))
			return result="*Age should be number";
		else if( !isDouble(amountdue))
			return result="*Amount due should be Number";
		else if(Integer.parseInt(age)<18 || Integer.parseInt(age)>120) 
				return result="*Age should be between 18 to 121";
		else if(Double.parseDouble(amountdue)<0|| Double.parseDouble(amountdue)>400000)
			return result="*Amount due should be between 0 and 400,000";
		else
			return "Success";
	}

	private boolean isDouble(String s) {
		try { 
	        Double.parseDouble(s); 
		} catch(NumberFormatException e) { 
	        return false; 
	    } catch(NullPointerException e) {
	        return false;
	    }
	    // only got here if we didn't return false
	    return true;
	}

	private boolean isNumeric(String s) {
		try { 
	        Integer.parseInt(s); 
	    } catch(NumberFormatException e) { 
	        return false; 
	    } catch(NullPointerException e) {
	        return false;
	    }
	    // only got here if we didn't return false
	    return true;
	}
}
