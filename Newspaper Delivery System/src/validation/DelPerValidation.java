package validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DelPerValidation {
	
	
	public boolean checkValidLength(int id, String name, String gender, String age,String address, String area, String uname, String pass)
	{	
		boolean result =true;
		if(name =="" || age =="" || pass =="" || gender =="" || uname =="" || address =="" || area ==""){
			System.out.print("error empty");
			return false;
		}else if(name.length()>40 ||name.length()<3 || 
				address.length()>150|| address.length()<3 ||
				area.length()>50||area.length()<3 || 
				uname.length()<3 || uname.length()>20 || 
				pass.length()<8 || pass.length()>25){
			System.out.println("error String length");
			System.out.println(name.length()+" "+address.length()+" "+area.length()+" "+uname.length()+" "+pass.length());
			return false;
		}
		else if (!Pattern.matches("[a-zA-Z ]+",name)) {
			System.out.print(name);
			return false;
		}
		else if(!isNumeric(age))
		{
			System.out.print("error is numbric");
			return false;
		}else if(Integer.parseInt(age)<18 || Integer.parseInt(age)>120)
		{
			System.out.print("error val");
			return false;
		}
		else if(!UsernameValidate(uname)){
			return false;
		}
		else if(!pass.matches("(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}") ){
			System.out.print("error Rex");
			return false;	
		}
		else{
			return true;
		}
	}

	private boolean UsernameValidate(String uname) {
		// TODO Auto-generated method stub
		Pattern pattern = Pattern.compile("[A-Za-z0-9_]+");
		Matcher matcher = pattern.matcher(uname);
			  return matcher.matches()?true:false;
	}

	private boolean isNumeric(String s) {
		try { 
	        Integer.parseInt(s); 
			//System.out.print("Int Pass");
	    } catch(NumberFormatException e) { 
	        return false; 
	    } catch(NullPointerException e) {
	        return false;
	    }
	    // only got here if we didn't return false
	    return true;
	}
	
}
