import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import Data.CustomerData;
import Data.DelPerData;
import services.CustomerService;
import services.DelPerService;

import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.JTree;
import java.awt.Canvas;
import java.awt.Font;
import java.awt.Component;

public class DeliveryPerson {

	public JFrame frame;
	private JTable table;
	private int id=0;
	private JTextField Id1;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField Uname;
	private JTextField password;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DeliveryPerson window = new DeliveryPerson();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public DeliveryPerson() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 916, 502);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblCreateANew = new JLabel("CREATE A NEW Delivery Person");
		lblCreateANew.setForeground(Color.GREEN);
		lblCreateANew.setBounds(10, 410, 337, 14);
		frame.getContentPane().add(lblCreateANew);
		
		JButton btnNewButton_2 = new JButton("EXIT");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RMIClientMain gui = new RMIClientMain();

				gui.frame.setVisible(true);
				frame.dispose();
			}
		});
		btnNewButton_2.setBounds(614, 380, 150, 49);
		frame.getContentPane().add(btnNewButton_2);
	        
		// Initializing the JTable 
		table = new JTable();
		createTable(table);
		table.setDefaultEditor(Object.class, null);


		JScrollPane scrollPane = new JScrollPane(table);
	    scrollPane.setBounds(10, 94, 842, 211);
	    
		frame.getContentPane().add(scrollPane);
		
		JTextField name = new JTextField();
		name.setBounds(10, 40, 140, 20);
		name.setText("name");
		placehlder(name,name.getText());
		frame.getContentPane().add(name);
		name.setColumns(10);
		
		String[] gender_data1 = { "Male", "Female" ,"Other" };
		JComboBox gender = new JComboBox(gender_data1);
		gender.setBounds(152, 40, 80, 20);
		gender.setSelectedIndex(0);
		frame.getContentPane().add(gender);
		
		JTextField age = new JTextField();
		age.setBounds(237, 40, 40, 20);
		age.setText("age");
		placehlder(age,age.getText());
		frame.getContentPane().add(age);
		age.setColumns(10);
		
		String[] availability_data1 = { "Available", "Not-Available" };
		
		JTextField Area = new JTextField();
		Area.setBounds(276, 40, 130, 20);
		Area.setText("Area");
		placehlder(Area,Area.getText());
		frame.getContentPane().add(Area);
		Area.setColumns(10);
		
		JTextArea Address = new JTextArea();
		Address.setBounds(409, 38, 150, 49);
		Address.setText("Address");
		placehlder(Address,Address.getText());
		frame.getContentPane().add(Address);

		Id1 = new JTextField();
		Id1.setDisabledTextColor(Color.WHITE);
		Id1.setAlignmentY(Component.BOTTOM_ALIGNMENT);
		Id1.setAlignmentX(Component.RIGHT_ALIGNMENT);
		Id1.setFont(new Font("Tahoma", Font.BOLD, 16));
		Id1.setBounds(10, 0, 40, 29);
		frame.getContentPane().add(Id1);
		Id1.setColumns(10);
		
		
		
		JButton btnInsert = new JButton("Insert");
		btnInsert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DelPerService dps = new DelPerService();
				String aa = " ";
				aa= dps.createDelPerson(id,name.getText(),gender.getSelectedItem().toString(),age.getText(),Address.getText(),Area.getText(),Uname.getText(),password.getText());

				if(aa== "Enter the Valid Field" || aa=="Sql error"||aa=="Error in Deleting" ||aa=="Row not found"){
					lblCreateANew.setText(aa);
					lblCreateANew.setForeground(Color.red);
				}
				else{
					lblCreateANew.setText(aa);
					lblCreateANew.setForeground(Color.GREEN);
				}
				createTable(table);
				frame.repaint();
			}
		});
		btnInsert.setBounds(10, 327, 89, 39);
		frame.getContentPane().add(btnInsert);
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DelPerService dps = new DelPerService();
				String aa = " ";
				try {
					aa= dps.updateCustomer(id,name.getText(),gender.getSelectedItem().toString(),age.getText(),Address.getText(),Area.getText(),Uname.getText(),password.getText());
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if(aa== "Enter the Valid Field" || aa=="Sql error" ||aa=="Error in Deleting"){
					
					lblCreateANew.setForeground(Color.red);
					lblCreateANew.setText(aa);
				}
				else{
					lblCreateANew.setText(aa);
					lblCreateANew.setForeground(Color.green);
				}
				createTable(table);
				frame.repaint();
				name.setText("");
				age.setText("");
				Uname.setText("");
				password.setText("");
				Area.setText("");

				Address.setText("");
				Id1.setText("");
			}
		});
		btnUpdate.setBounds(109, 327, 98, 39);
		frame.getContentPane().add(btnUpdate);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DelPerService dps = new DelPerService();
				lblCreateANew.setText(dps.deleteDeliveryPerson(id));
				createTable(table);
				frame.repaint();
			}
		});
		btnDelete.setBounds(216, 327, 90, 39);
		frame.getContentPane().add(btnDelete);
		
		Uname = new JTextField();
		Uname.setBounds(562, 40, 98, 20);
		Uname.setColumns(10);
		Uname.setText("Username");
		placehlder(Uname,Uname.getText());
		frame.getContentPane().add(Uname);
		
		password = new JTextField();
		password.setBounds(658, 40, 106, 20);
		password.setText("password");
		placehlder(password,password.getText());
		frame.getContentPane().add(password);
		password.setColumns(10);
		
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				DefaultTableModel model = (DefaultTableModel)table.getModel();

		        // get the selected row index
		       int selectedRowIndex = table.getSelectedRow();
		        // set the selected row data into jtextfields
		       id = Integer.parseInt(model.getValueAt(selectedRowIndex, 7).toString());
		       Id1.setText(model.getValueAt(selectedRowIndex, 7).toString());
		       name.setText(model.getValueAt(selectedRowIndex, 0).toString());
		       if(model.getValueAt(selectedRowIndex, 1).toString().charAt(0) == 'M')
		    	   gender.setSelectedIndex(0);
		       else if(model.getValueAt(selectedRowIndex, 1).toString().charAt(0) == 'F')
		    	   gender.setSelectedIndex(1);
		       else
		    	   gender.setSelectedIndex(2);
		       
		       age.setText(model.getValueAt(selectedRowIndex, 2).toString());
		       Area.setText(model.getValueAt(selectedRowIndex, 3).toString());
		       Address.setText(model.getValueAt(selectedRowIndex, 4).toString());
		       Uname.setText(model.getValueAt(selectedRowIndex, 5).toString());
		       password.setText(model.getValueAt(selectedRowIndex, 6).toString());
			}
		});
	}

	private void createTable(JTable table) {
		// TODO Auto-generated method stub
		DelPerService delperservice = new DelPerService();
		ArrayList<DelPerData> cd =delperservice.viewDeliveryPerson();
		
		DefaultTableModel model = new DefaultTableModel();
		Object[] columnsName = new Object[8];
        columnsName[0] = "Name";
        columnsName[1] = "gender";
        columnsName[2] = "Age";
        columnsName[3] = "Responsible Area";
        columnsName[4] = "Address";
        columnsName[5] = "UserName";
        columnsName[6] = "password";
        columnsName[7] = "Id";
        model.setColumnIdentifiers(columnsName);
        
		Object[] rowData = new Object[8];
		for(int i = 0; i < cd.size(); i++){
        	rowData[0] = cd.get(i).getDelName();
        	rowData[1] = cd.get(i).getDelGender();
        	rowData[2] = cd.get(i).getDelAge();
        	rowData[3] = cd.get(i).getDelArea();
        	rowData[4] = cd.get(i).getDelAddress();
        	rowData[5] = cd.get(i).getDelUsername();
        	rowData[6] = cd.get(i).getDelPassword();
        	rowData[7] = cd.get(i).getDelId();
        	model.addRow(rowData);
        }
		table.setModel(model);
			table.setName("Delivery Person Data"); 
			table.setCellSelectionEnabled(false);
			table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
			table.getColumnModel().getColumn(0).setPreferredWidth(140);
			table.getColumnModel().getColumn(1).setPreferredWidth(60);
			table.getColumnModel().getColumn(2).setPreferredWidth(60);
			table.getColumnModel().getColumn(3).setPreferredWidth(130);
			table.getColumnModel().getColumn(4).setPreferredWidth(180);
			table.getColumnModel().getColumn(5).setPreferredWidth(130);
			table.getColumnModel().getColumn(6).setPreferredWidth(130);
			table.getColumnModel().getColumn(7).setPreferredWidth(0);
	}
	
	private void placehlder(JTextField o1,String s2) {
		// TODO Auto-generated method stub
		o1.setToolTipText(s2);
		o1.addMouseListener(new java.awt.event.MouseAdapter(){
			public void mouseEntered(java.awt.event.MouseEvent evt) {
				
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		    }
		});
		o1.addFocusListener(new FocusListener(){
			
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				 if (o1.getText().equals(s2)) {
					 o1.setText("");
					 o1.setForeground(Color.BLACK);
			        }
			}

			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				 if (o1.getText().isEmpty()) {
					 o1.setForeground(Color.GRAY);
					 o1.setText(s2);
			        }
			}
			
		});
	}
	private void placehlder(JTextArea o1,String s2) {
		// TODO Auto-generated method stub
		o1.setToolTipText(s2);
		o1.addMouseListener(new java.awt.event.MouseAdapter(){
			public void mouseEntered(java.awt.event.MouseEvent evt) {
				
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		    }
		});
		o1.addFocusListener(new FocusListener(){
			
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				 if (o1.getText().equals(s2)) {
					 o1.setText("");
					 o1.setForeground(Color.BLACK);
			        }
			}

			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				 if (o1.getText().isEmpty()) {
					 o1.setForeground(Color.GRAY);
					 o1.setText(s2);
			        }
			}
			
		});
	}
	
}