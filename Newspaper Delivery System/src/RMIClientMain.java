import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.SystemColor;
import java.awt.Component;

public class RMIClientMain extends JFrame {
	public JFrame frame;
 	public RMIClientMain(){
		ImageIcon icon=new ImageIcon("back1.jpg");
		JLabel label=new JLabel(icon);
		label.setBounds(0,0,icon.getIconWidth(),icon.getIconHeight());
		frame = new JFrame();
		frame.getLayeredPane().add(label,new Integer(Integer.MIN_VALUE));
		JPanel j=(JPanel)frame.getContentPane();
		j.setOpaque(false);JPanel panel=new JPanel();
		panel.setFocusTraversalPolicyProvider(true);
		panel.setLayout(null);
		panel.setOpaque(false);
		frame.getContentPane().add(panel);
		frame.getContentPane().add(panel);
		JLabel lblNewLabel = new JLabel("Newspaper Delivery ");
		lblNewLabel.setForeground(SystemColor.controlDkShadow);
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 42));
		lblNewLabel.setBounds(521, 47, 389, 115);
		panel.add(lblNewLabel);
		JButton btnNewButton = new JButton("LOGIN");
		btnNewButton.setAlignmentY(Component.TOP_ALIGNMENT);
		btnNewButton.setBackground(SystemColor.controlHighlight);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name="Yash";
				CustomerGui gui = new CustomerGui(name);
				gui.frame.setVisible(true);
				frame.dispose();
			}
		});
		btnNewButton.setFont(new Font("Segoe UI Emoji", Font.BOLD, 16));
		btnNewButton.setBounds(613, 359, 198, 43);
		panel.add(btnNewButton);
		
		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnExit.setFont(new Font("Segoe UI Emoji", Font.BOLD, 16));
		btnExit.setBackground(SystemColor.controlShadow);
		btnExit.setBounds(613, 445, 198, 43);
		panel.add(btnExit);
		
		JLabel lblSystem = new JLabel("System");
		lblSystem.setForeground(SystemColor.controlDkShadow);
		lblSystem.setFont(new Font("Times New Roman", Font.BOLD, 42));
		lblSystem.setBounds(637, 146, 150, 81);
		panel.add(lblSystem);
		frame.setSize(936,527);
		frame.setVisible(true);
		}
	public static void main(String[] args) 
	{	
		new RMIClientMain();
	}
}
