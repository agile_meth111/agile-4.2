package impl;

import Data.CustomerData;
import Data.DelPerData;
import JDBC.CreateConnection;
import interfaces.DelPerDao;

import java.sql.*;
import java.util.ArrayList;


public class DelPerImpl implements DelPerDao{

	Connection con = CreateConnection.getDriver();
	private String viewQuery = "Select * from create_del_person";
	private String deletequery="delete from create_del_person where delid=?";
	
	ArrayList<DelPerData> deldata = new ArrayList<DelPerData>();
	@Override
	public String adddelper(DelPerData DelPerData) {
		int count = 0;
		String insertQuery ="insert into create_del_person(Name,Gender,Age,Address,`Responsible Area`,UserName,Password)" + "values(?,?,?,?,?,?,?)";
		
		
				try {
					PreparedStatement pstmt =con.prepareStatement(insertQuery);
					pstmt.setString(1, DelPerData.getDelName());
					pstmt.setString(2, DelPerData.getDelGender());
					pstmt.setString(3, DelPerData.getDelAge()+"");
					pstmt.setString(4, DelPerData.getDelAddress());
					pstmt.setString(5, DelPerData.getDelArea());
					pstmt.setString(6, DelPerData.getDelUsername());
					pstmt.setString(7, DelPerData.getDelPassword());
					count = pstmt.executeUpdate();
					pstmt.close();
					System.out.println("success");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return "Sql error";
				}
				return "Inserted";
		
	}
	@Override
	public String update(DelPerData customer1) throws Exception{
		DelPerData delivery = customer1;
		int count = 0;
		Connection con = CreateConnection.getDriver();
        String sql="UPDATE create_del_person SET Name=?, Gender=?, Age=?, Address=?, `Responsible Area`=?, Username=?, Password=? WHERE delid=?";
        try {
			PreparedStatement pstmt =con.prepareStatement(sql);
			//pstmt.setInt(1, delivery.getDelId());
			pstmt.setString(1, delivery.getDelName());
			pstmt.setString(2, delivery.getDelGender());
			pstmt.setString(3, delivery.getDelAge()+"");
			pstmt.setString(4, delivery.getDelAddress());
			pstmt.setString(5, delivery.getDelArea());
			pstmt.setString(6, delivery.getDelUsername());
			pstmt.setString(7, delivery.getDelPassword());
			pstmt.setString(8, delivery.getDelId()+"");
			count = pstmt.executeUpdate();
			System.out.println(count);
			if(count<1)
				return "Row not found";
			pstmt.close();
        } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "Sql error";
		}
		return "Updated";
	}
	@Override
	public DelPerData findById(int id) {
		DelPerData delivery = null;

		String strSQL = "select * from create_del_person where delid = ?";

		try {
			PreparedStatement pstmt = con.prepareStatement(strSQL);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				delivery.setDelId(rs.getInt("delid"));
				delivery.setDelName(rs.getString("Name"));
				delivery.setDelGender(rs.getString("Gender"));
				delivery.setDelAge(rs.getInt("Age"));
				delivery.setDelAddress(rs.getString("Address"));
				delivery.setDelArea(rs.getString("Responsible Area"));
				delivery.setDelUsername(rs.getString("UserName"));
				delivery.setDelPassword(rs.getString("Password"));
			}
			pstmt.close();
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return delivery;
	}
	@Override
	public String detele(int id) throws SQLException {
		try {
			PreparedStatement ps =con.prepareStatement(deletequery);
			ps.setString(1, id+"");
			int count = ps.executeUpdate();
			System.out.println(count);
			if(count<1)
				return "Row not found";
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return("Sql Error");
		}
		
		return "Deleted";
	}
	public ArrayList<DelPerData> viewDeliveryPerson() {
		// TODO Auto-generated method stub
		try {
			Statement statement = con.createStatement();
			ResultSet result = statement.executeQuery(viewQuery);
			while(result.next()){
				DelPerData d = new DelPerData(
						result.getInt("delid"),
						result.getString("Name"),
						result.getString("gender"),
						result.getInt("age"),
						result.getString("address"),
						result.getString("Responsible Area"),
						result.getString("UserName"),
						result.getString("Password")
				);
				deldata.add(d);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			deldata.add(null);
			e.printStackTrace();
		}
		
		return deldata;
	}


}