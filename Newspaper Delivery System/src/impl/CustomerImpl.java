package impl;
import java.util.ArrayList;
import java.sql.*;

import Data.CustomerData;
import interfaces.CustomerDao;
import JDBC.*;
public class CustomerImpl implements CustomerDao{
	private Connection con = CreateConnection.getDriver();
	private String insertQuery ="INSERT INTO customers (name,gender, address, area, availibity, amount_due,age) VALUES(?,?,?,?,?,?,?)";
	private String viewQuery = "Select * from customers";
	private String update ="UPDATE Customers SET name=?,gender=?, address=?, area=?, availibity=?, amount_due=?,age=? WHERE id = ?";
	private String deletequery="DELETE FROM customers WHERE customers.Id = ? ";
	ArrayList<CustomerData> custdata = new ArrayList<CustomerData>();
	@Override
	public String createProduct(CustomerData customer) {
		// TODO Auto-generated method stub
			try {
				PreparedStatement ps =con.prepareStatement(insertQuery);
				ps.setString(1, customer.getCustname());
				ps.setString(2, customer.getGender()+"");
				ps.setString(3, customer.getCustAddress());
				ps.setString(4, customer.getArea());
				ps.setString(5, customer.getAvailibilty()+"");
				ps.setString(6, customer.getAmountDue()+"");
				ps.setString(7, customer.getCustAge()+"");
				ps.executeUpdate();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "Sql error";
			}
			return "Inserted";
	}
	@Override
	public ArrayList<CustomerData> viewCustomer() {
		// TODO Auto-generated method stub
		try {
			Statement statement = con.createStatement();
			ResultSet result = statement.executeQuery(viewQuery);
			//Select name,gender,age,address,area,availibity,amountdue from customers
			while(result.next()){
				CustomerData c = new CustomerData(
						result.getInt("Id"),
						result.getString("Name"),
						result.getInt("age"),
						result.getDouble("amount_due"),
						(result.getString("gender")+"  ").charAt(0),
						result.getInt("availibity"),
						result.getString("address"),
						result.getString("area")
					);
				custdata.add(c);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			custdata.add(null);
			e.printStackTrace();
		}
		
		return custdata;
	}
	public String updateCustomer(CustomerData customer){
		try {
			PreparedStatement ps =con.prepareStatement(update);
			ps.setString(1, customer.getCustname());
			ps.setString(2, customer.getGender()+"");
			ps.setString(3, customer.getCustAddress());
			ps.setString(4, customer.getArea());
			ps.setString(5, customer.getAvailibilty()+"");
			ps.setString(6, customer.getAmountDue()+"");
			ps.setString(7, customer.getCustAge()+"");
			ps.setString(8, customer.getId()+"");
			int count = ps.executeUpdate();
			System.out.println(count);
			if(count<1)
				return "Row not found";
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "Sql error";
		}
		return "Updated";
	}
	public String deleteCustomer(int id){
		try {
			PreparedStatement ps =con.prepareStatement(deletequery);
			ps.setString(1, id+"");
			int count = ps.executeUpdate();
			System.out.println(count);
			if(count<1)
				return "Row not found";
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return("Sql Error");
		}
		
		return "Deleted";
		
	}
}
