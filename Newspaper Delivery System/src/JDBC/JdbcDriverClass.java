package JDBC;

import java.sql.*;

public class JdbcDriverClass {
	private static Connection con; 
	public static void main(String args[]){  
		try{  
		Class.forName("com.mysql.jdbc.Driver");  
		con=CreateConnection.getDriver();  
		Statement stmt=con.createStatement();
		ResultSet rs=stmt.executeQuery("select * from customers");  
		while(rs.next())  
			System.out.println(rs.getInt(1)+"  "+rs.getString(2)+"  "+rs.getString(3));  
		con.close();  
		}catch(Exception e){ System.out.println(e);}  
		}  
}
