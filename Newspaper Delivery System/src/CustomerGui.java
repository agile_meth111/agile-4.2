import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Image;
import java.awt.ScrollPane;
import java.awt.Toolkit;
import java.util.ArrayList;

import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import Data.CustomerData;
import services.CustomerService;

import javax.swing.JScrollPane;
import javax.swing.JTable;

import java.awt.Component;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;
import java.awt.FlowLayout;
import java.awt.SystemColor;
import java.awt.event.MouseMotionAdapter;

public class CustomerGui {

	JFrame frame;
	private JTextField name;
	private JTextField age;
	private JTextField amount_due;
	private JTextField Area;
	private JTextField Id1;
	private JTable table;
	private int id=0;
	private JTextField txtSearchByValue;
	private TableRowSorter sorter;
	private String name1;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CustomerGui window = new CustomerGui();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CustomerGui() {
		initialize();
	}	
	public CustomerGui(String name) {
		name1=name;
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Newspaper delivery System");
		frame.getContentPane().setBackground(Color.WHITE);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setSize(screenSize.width, screenSize.height);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setForeground(new Color(0, 0, 51));
		panel.setBackground(new Color(51, 51, 51));
		panel.setBounds(0, 67, 283, 662);
		frame.getContentPane().add(panel);
		
		JLabel lblNewLabel = new JLabel(name1);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Arial Black", Font.BOLD, 16));
		lblNewLabel.setBackground(Color.BLACK);
		lblNewLabel.setBounds(61, 127, 142, 30);
		panel.add(lblNewLabel);
		
		JLabel jl=new JLabel();
	    jl.setIcon(new javax.swing.ImageIcon(getClass().getResource("UserLogo1.png")));
	    jl.setBounds(61, 11, 147, 119);
	    panel.add(jl);
		
		JButton Delivery = new JButton("Delivery Person");
		Delivery.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			/*	DeliveryPersonGUI gui = new DeliveryPersonGUI();
				gui.frame.setVisible(true);
				frame.dispose();
				*/
			}
		});
		Delivery.setHorizontalAlignment(SwingConstants.LEFT);
		Delivery.setForeground(new Color(204, 204, 204));
		Delivery.setFont(new Font("SansSerif", Font.BOLD | Font.ITALIC, 18));
		Delivery.setFocusPainted(false);
		Delivery.setBorder(new LineBorder(new Color(51, 51, 51)));
		Delivery.setBackground(new Color(62, 62, 62));
		Delivery.setBounds(0, 216, 263, 37);
		panel.add(Delivery);
		
		JButton Subscription = new JButton("Suscription");
		Subscription.setHorizontalAlignment(SwingConstants.LEFT);
		Subscription.setForeground(new Color(204, 204, 204));
		Subscription.setFont(new Font("SansSerif", Font.BOLD | Font.ITALIC, 18));
		Subscription.setFocusPainted(false);
		Subscription.setBorder(new LineBorder(new Color(51, 51, 51)));
		Subscription.setBackground(new Color(62, 62, 62));
		Subscription.setBounds(0, 264, 263, 37);
		panel.add(Subscription);
		
		JButton Customer = new JButton("Manage Customer");
		Customer.setHorizontalAlignment(SwingConstants.LEFT);
		Customer.setForeground(new Color(204, 204, 204));
		Customer.setFont(new Font("SansSerif", Font.BOLD | Font.ITALIC, 18));
		Customer.setFocusPainted(false);
		Customer.setBorder(new LineBorder(new Color(51, 51, 51)));
		Customer.setBackground(new Color(62, 82, 72));
		Customer.setBounds(0, 168, 263, 37);
		panel.add(Customer);
		
		JButton Publication = new JButton("Publication");
		Publication.setHorizontalAlignment(SwingConstants.LEFT);
		Publication.setForeground(new Color(204, 204, 204));
		Publication.setFont(new Font("SansSerif", Font.BOLD | Font.ITALIC, 18));
		Publication.setFocusPainted(false);
		Publication.setBorder(new LineBorder(new Color(51, 51, 51)));
		Publication.setBackground(new Color(62, 62, 62));
		Publication.setBounds(0, 313, 263, 37);
		panel.add(Publication);
		
		JButton btnInvoice = new JButton("Invoice");
		btnInvoice.setHorizontalAlignment(SwingConstants.LEFT);
		btnInvoice.setForeground(new Color(204, 204, 204));
		btnInvoice.setFont(new Font("SansSerif", Font.BOLD | Font.ITALIC, 18));
		btnInvoice.setFocusPainted(false);
		btnInvoice.setBorder(new LineBorder(new Color(51, 51, 51)));
		btnInvoice.setBackground(new Color(62, 62, 62));
		btnInvoice.setBounds(0, 361, 263, 37);
		panel.add(btnInvoice);
		
		JButton btnDeliveryArea = new JButton("Delivery Area");
		btnDeliveryArea.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnDeliveryArea.setHorizontalAlignment(SwingConstants.LEFT);
		btnDeliveryArea.setForeground(new Color(204, 204, 204));
		btnDeliveryArea.setFont(new Font("SansSerif", Font.BOLD | Font.ITALIC, 18));
		btnDeliveryArea.setFocusPainted(false);
		btnDeliveryArea.setBorder(new LineBorder(new Color(51, 51, 51)));
		btnDeliveryArea.setBackground(new Color(62, 62, 62));
		btnDeliveryArea.setBounds(0, 409, 263, 37);
		panel.add(btnDeliveryArea);
		
		JButton btnChangeUser = new JButton("Change User");
		btnChangeUser.setHorizontalAlignment(SwingConstants.LEFT);
		btnChangeUser.setForeground(new Color(204, 204, 204));
		btnChangeUser.setFont(new Font("SansSerif", Font.BOLD | Font.ITALIC, 18));
		btnChangeUser.setFocusPainted(false);
		btnChangeUser.setBorder(new LineBorder(new Color(51, 51, 51)));
		btnChangeUser.setBackground(new Color(62, 62, 62));
		btnChangeUser.setBounds(0, 457, 263, 37);
		panel.add(btnChangeUser);
		
		JPanel panel_1 = new JPanel();
		panel_1.setName("Customer");
		panel_1.setBackground(Color.WHITE);
		panel_1.setBounds(283, 150, 1077, 589);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		txtSearchByValue = new JTextField();
		txtSearchByValue.setFont(new Font("Times New Roman", Font.BOLD, 14));
		txtSearchByValue.setToolTipText("Enter Key word to be searched");
		txtSearchByValue.setBounds(10, 207, 259, 20);
		panel_1.add(txtSearchByValue);
		txtSearchByValue.setColumns(10);
		
		table = new JTable();
		createTable(table);
		table.setDefaultEditor(Object.class, null);
		table.setRowHeight(24);
		JScrollPane scrollPane = new JScrollPane(table);
	    scrollPane.setBounds(10, 235, 1043, 295);	    
	    panel_1.add(scrollPane);
		
		Border roundedBorder = new LineBorder(Color.BLACK, 1, true);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(Color.DARK_GRAY);
		panel_3.setBounds(10, 0, 571, 176);
		panel_1.add(panel_3);
		panel_3.setLayout(null);
		
		name = new JTextField();
		name.setBounds(99, 38, 140, 20);
		panel_3.add(name);
		name.setToolTipText("name");
		name.setColumns(10);
		
		JComboBox gender = new JComboBox(new Object[]{ "Male", "Female" ,"Other" });
		gender.setBounds(99, 68, 140, 20);
		panel_3.add(gender);
		gender.setSelectedIndex(0);
		
		age = new JTextField();
		age.setBounds(99, 99, 140, 20);
		panel_3.add(age);
		age.setColumns(10);
		
		JComboBox availability = new JComboBox(new Object[]{"Available", "Not-Available" });
		availability.setBounds(99, 130, 140, 20);
		panel_3.add(availability);
		availability.setSelectedIndex(0);
		
		amount_due = new JTextField();
		amount_due.setBounds(412, 11, 146, 20);
		panel_3.add(amount_due);
		amount_due.setColumns(10);
		
		Area = new JTextField();
		Area.setBounds(412, 36, 146, 20);
		panel_3.add(Area);
		Area.setColumns(10);
		
		Id1 = new JTextField();
		Id1.setBounds(99, 11, 40, 20);
		panel_3.add(Id1);
		Id1.setBackground(SystemColor.menu);
		Id1.setFont(new Font("Tahoma", Font.BOLD, 16));
		Id1.setDisabledTextColor(Color.WHITE);
		Id1.setColumns(10);
		Id1.setAlignmentY(1.0f);
		Id1.setAlignmentX(1.0f);
		
		JLabel lblNewLabel_1 = new JLabel("Name: ");
		lblNewLabel_1.setBounds(10, 38, 79, 17);
		panel_3.add(lblNewLabel_1);
		lblNewLabel_1.setForeground(Color.GRAY);
		lblNewLabel_1.setFont(new Font("Times New Roman", Font.BOLD, 14));
		
		JLabel lblNewLabel_1_1 = new JLabel("Gender:");
		lblNewLabel_1_1.setForeground(Color.GRAY);
		lblNewLabel_1_1.setBounds(10, 68, 79, 17);
		panel_3.add(lblNewLabel_1_1);
		lblNewLabel_1_1.setFont(new Font("Times New Roman", Font.BOLD, 14));
		
		JLabel lblNewLabel_1_2 = new JLabel("AGE: ");
		lblNewLabel_1_2.setForeground(Color.GRAY);
		lblNewLabel_1_2.setBounds(10, 99, 79, 17);
		panel_3.add(lblNewLabel_1_2);
		lblNewLabel_1_2.setFont(new Font("Times New Roman", Font.BOLD, 14));
		
		JLabel lblNewLabel_1_3 = new JLabel("Availbility:");
		lblNewLabel_1_3.setForeground(Color.GRAY);
		lblNewLabel_1_3.setBounds(10, 130, 79, 17);
		panel_3.add(lblNewLabel_1_3);
		lblNewLabel_1_3.setFont(new Font("Times New Roman", Font.BOLD, 14));
		
		JLabel lblNewLabel_1_4 = new JLabel("Amount Due:");
		lblNewLabel_1_4.setForeground(Color.GRAY);
		lblNewLabel_1_4.setBounds(312, 12, 90, 17);
		panel_3.add(lblNewLabel_1_4);
		lblNewLabel_1_4.setFont(new Font("Times New Roman", Font.BOLD, 14));
		
		JLabel lblNewLabel_1_5 = new JLabel("Area:");
		lblNewLabel_1_5.setForeground(Color.GRAY);
		lblNewLabel_1_5.setBounds(312, 37, 79, 17);
		panel_3.add(lblNewLabel_1_5);
		lblNewLabel_1_5.setFont(new Font("Times New Roman", Font.BOLD, 14));
		
		JLabel lblNewLabel_1_6 = new JLabel("Address:");
		lblNewLabel_1_6.setForeground(Color.GRAY);
		lblNewLabel_1_6.setBounds(312, 67, 79, 17);
		panel_3.add(lblNewLabel_1_6);
		lblNewLabel_1_6.setFont(new Font("Times New Roman", Font.BOLD, 14));
		
		JLabel lblNewLabel_1_7 = new JLabel("Id:");
		lblNewLabel_1_7.setBounds(10, 14, 79, 17);
		panel_3.add(lblNewLabel_1_7);
		lblNewLabel_1_7.setForeground(Color.GRAY);
		lblNewLabel_1_7.setFont(new Font("Times New Roman", Font.BOLD, 14));
		
		JTextArea Address = new JTextArea();
		Address.setToolTipText("Address");
		JScrollPane scrollPane1 = new JScrollPane(Address);
		scrollPane1.setBounds(410, 65, 148, 83);	 
		scrollPane1.setBorder(roundedBorder);
		panel_3.add(scrollPane1);
		
		JLabel lblCreateANew = new JLabel("");
		lblCreateANew.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCreateANew.setBounds(10, 158, 308, 17);
		panel_3.add(lblCreateANew);
		lblCreateANew.setForeground(Color.GREEN);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBackground(Color.DARK_GRAY);
		panel_4.setBounds(591, 0, 462, 176);
		panel_1.add(panel_4);
		panel_4.setLayout(null);
		
			JButton btnInsert = new JButton("Add Customer");
			btnInsert.setFont(new Font("Times New Roman", Font.BOLD, 12));
			btnInsert.setBounds(10, 12, 121, 39);
			panel_4.add(btnInsert);
			
			JButton btnUpdate = new JButton("Modify Customer");
			btnUpdate.setFont(new Font("Times New Roman", Font.BOLD, 12));
			btnUpdate.setBounds(141, 12, 151, 39);
			panel_4.add(btnUpdate);
			
			JButton btnDelete = new JButton(" Delete Customer");
			btnDelete.setFont(new Font("Times New Roman", Font.BOLD, 12));
			btnDelete.setBounds(302, 11, 151, 39);
			panel_4.add(btnDelete);
			
			JButton btnNewButton = new JButton("Clear all the Feilds");
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					name.setText("");
					age.setText("");
					amount_due.setText("");
					Area.setText("");
					Address.setText("");
					Id1.setText("");
					createTable(table);
				}
			});
			btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 16));
			btnNewButton.setBounds(10, 62, 443, 39);
			panel_4.add(btnNewButton);
			
			JButton btnNewButton_2 = new JButton("EXIT");
			btnNewButton_2.setFont(new Font("Times New Roman", Font.BOLD, 18));
			btnNewButton_2.setBounds(10, 112, 443, 44);
			panel_4.add(btnNewButton_2);
			
			JLabel lblNewLabel_2 = new JLabel("Search  for Delivery Person Details ");
			lblNewLabel_2.setFont(new Font("Times New Roman", Font.BOLD, 16));
			lblNewLabel_2.setBounds(10, 187, 354, 14);
			panel_1.add(lblNewLabel_2);
			
			
			btnDelete.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					CustomerService cs1 = new CustomerService();
					lblCreateANew.setText(cs1.deletCustomer(id));
					
					name.setText("");
					age.setText("");
					amount_due.setText("");
					Area.setText("");
					Address.setText("");
					Id1.setText("");
					createTable(table);
					frame.repaint();
				}
			});
			btnUpdate.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					CustomerService cs1 = new CustomerService();
					String aa = " ";
					aa= cs1.updateCustomer(id,name.getText(),age.getText(),amount_due.getText(),gender.getSelectedItem().toString(),availability.getSelectedItem().toString(),Address.getText(),Area.getText());
					
					if(aa== "Updated Successful"){
						lblCreateANew.setText("Modified Successfully");
						lblCreateANew.setForeground(Color.GREEN);
						name.setText("");
						age.setText("");
						amount_due.setText("");
						Area.setText("");
						Address.setText("");
						Id1.setText("");
						createTable(table);
						frame.repaint();
						id=0;
					}
					else{
						lblCreateANew.setText(aa);
						lblCreateANew.setForeground(Color.red);
					}
				}
			});
			btnInsert.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					CustomerService cs1 = new CustomerService();
					String aa = " ";
					aa= cs1.createCustomer(name.getText(),age.getText(),amount_due.getText(),gender.getSelectedItem().toString(),availability.getSelectedItem().toString(),Address.getText(),Area.getText());

					if(aa== "Insert Successful"){
						lblCreateANew.setText("Added Successfully");
						lblCreateANew.setForeground(Color.GREEN);
						name.setText("");
						age.setText("");
						amount_due.setText("");
						Area.setText("");
						Address.setText("");
						Id1.setText("");
						
						createTable(table);
						frame.repaint();
						
						id=0;
					}
					else{
						lblCreateANew.setText(aa);
						lblCreateANew.setForeground(Color.red);
					}
					
				}
			});
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(51, 51, 51));
		panel_2.setBounds(0, 0, 1360, 67);
		frame.getContentPane().add(panel_2);
		panel_2.setLayout(null);
		
		JLabel companyNameLbl = new JLabel("Newspaper delivery System");
		companyNameLbl.setBounds(0, 0, 685, 59);
		panel_2.add(companyNameLbl);
		companyNameLbl.setForeground(Color.WHITE);
		companyNameLbl.setFont(new Font("Algerian", Font.BOLD, 44));
		companyNameLbl.setBackground(Color.YELLOW);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBounds(283, 67, 1077, 72);
		frame.getContentPane().add(panel_5);
		panel_5.setLayout(null);
		
		JLabel lblCustomer = new JLabel("Manage Customer Details");
		lblCustomer.setBounds(77, 18, 585, 54);
		panel_5.add(lblCustomer);
		lblCustomer.setForeground(SystemColor.desktop);
		lblCustomer.setFont(new Font("Algerian", Font.BOLD, 40));
		lblCustomer.setBackground(Color.YELLOW);
		
		JLabel jl_1 = new JLabel();
		jl_1.setBounds(0, 11, 67, 61);
		panel_5.add(jl_1);
	    ImageIcon imageIcon = new ImageIcon(new ImageIcon("customerlogo.png").getImage().getScaledInstance(67, 61, Image.SCALE_DEFAULT));
		jl_1.setIcon(imageIcon);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				DefaultTableModel model = (DefaultTableModel)table.getModel();

		        // get the selected row index
		       int selectedRowIndex = table.getSelectedRow();
		        // set the selected row data into jtextfields
		       id = Integer.parseInt(model.getValueAt(selectedRowIndex, 0).toString());
		       Id1.setText(model.getValueAt(selectedRowIndex, 0).toString());
		       name.setText(model.getValueAt(selectedRowIndex, 1).toString());
		       if(model.getValueAt(selectedRowIndex, 2).toString().charAt(0) == 'M')
		    	   gender.setSelectedIndex(0);
		       else if(model.getValueAt(selectedRowIndex, 2).toString().charAt(0) == 'F')
		    	   gender.setSelectedIndex(1);
		       else
		    	   gender.setSelectedIndex(2);
		       
		       age.setText(model.getValueAt(selectedRowIndex, 3).toString());
		       
		       if(model.getValueAt(selectedRowIndex, 4).toString() == "Available")
		    	   availability.setSelectedIndex(0);
		       else
		    	   availability.setSelectedIndex(1);
		       amount_due.setText(model.getValueAt(selectedRowIndex, 5).toString());
		       Area.setText(model.getValueAt(selectedRowIndex, 6).toString());
		       Address.setText(model.getValueAt(selectedRowIndex, 7).toString());
			}
		});
	}

	private void createTable(JTable table) {
		// TODO Auto-generated method stub
		CustomerService cs = new CustomerService();
		ArrayList<CustomerData> cd =cs.viewCustomer();
		
		DefaultTableModel model = new DefaultTableModel();
		Object[] columnsName = new Object[8];
		columnsName[0] = "Id";
        columnsName[1] = "Name";
        columnsName[2] = "gender";
        columnsName[3] = "Availibity";
        columnsName[4] = "Age";
        columnsName[5] = "amount_due";
        columnsName[6] = "Area";
        columnsName[7] = "Address";
        model.setColumnIdentifiers(columnsName);

        Object[] rowData = new Object[8];
		for(int i = 0; i < cd.size(); i++){
        	rowData[0] = cd.get(i).getId();
        	rowData[1] = cd.get(i).getCustname();
        	rowData[2] = cd.get(i).getGender();
        	rowData[3] = cd.get(i).getCustAge();
        	rowData[4] = (cd.get(i).getAvailibilty() == 1) ? "Available" : "Not-Available" ;
        	rowData[5] = cd.get(i).getAmountDue();
        	rowData[6] = cd.get(i).getArea();
        	rowData[7] = cd.get(i).getCustAddress();
        	model.addRow(rowData);

        }

		table.setModel(model);
		model.fireTableDataChanged();table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);		
		for (int i = 0; i < table.getColumnCount(); i++) {
	      DefaultTableColumnModel colModel = (DefaultTableColumnModel) table.getColumnModel();
	      TableColumn col = colModel.getColumn(i);
	      int width = 0;
	
	      TableCellRenderer renderer = col.getHeaderRenderer();
	      for (int r = 0; r < table.getRowCount()-1; r++) {
	        renderer = table.getCellRenderer(r, i);
	        Component comp = renderer.getTableCellRendererComponent(table, table.getValueAt(r, i),
	            false, false, r, i);
	        width = Math.max(width, comp.getPreferredSize().width);
	      }
	      col.setPreferredWidth(width + 2);
		}
		 TableRowSorter<TableModel> sorter = new TableRowSorter<>(table.getModel());
		    table.setRowSorter(sorter);
		    txtSearchByValue.getDocument().addDocumentListener(new DocumentListener(){
		    	@Override
		         public void insertUpdate(DocumentEvent e) {
		            search(txtSearchByValue.getText());
		         }
		         @Override
		         public void removeUpdate(DocumentEvent e) {
		            search(txtSearchByValue.getText());
		         }
		         @Override
		         public void changedUpdate(DocumentEvent e) {
		            search(txtSearchByValue.getText());
		         }
		         public void search(String str) {
		            if (str.length() == 0) {
		               sorter.setRowFilter(null);
		            } else {
		               sorter.setRowFilter(RowFilter.regexFilter(str));
		            }
		         }
		    });
			 

	}

		
	
}
