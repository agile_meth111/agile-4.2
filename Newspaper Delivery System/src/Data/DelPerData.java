package Data;

import java.io.Serializable;

public class DelPerData implements Serializable {
		private int DelId;
	    private String DelName;
	    private String DelGender;
	    private int DelAge;
	    private String DelAddress;
	    private String DelArea;
	    private String DelUsername;
	    private String DelPassword;
	   
		public DelPerData(int delId,String delName, String delGender, int delAge, String delAddress, String delArea,
				String delUsername, String delPassword) {
			super();
			this.DelId = delId;
			this.DelName = delName;
			this.DelGender = delGender;
			this.DelAge = delAge;
			this.DelAddress = delAddress;
			this.DelArea = delArea;
			this.DelUsername = delUsername;
			this.DelPassword = delPassword;
		}
		public DelPerData(String delName, String delGender, int delAge, String delAddress, String delArea,
				String delUsername, String delPassword) {
			super();
			
			this.DelName = delName;
			this.DelGender = delGender;
			this.DelAge = delAge;
			this.DelAddress = delAddress;
			this.DelArea = delArea;
			this.DelUsername = delUsername;
			this.DelPassword = delPassword;
		}
		
		

		public int getDelId() {
			return DelId;
		}
		public void setDelId(int delId) {
			this.DelId = delId;
		}
		public String getDelName() {
			return DelName;
		}
		public void setDelName(String delName) {
			this.DelName = delName;
		}
		public String getDelGender() {
			return DelGender;
		}
		public void setDelGender(String delGender) {
			this.DelGender = delGender;
		}
		public int getDelAge() {
			return DelAge;
		}
		public void setDelAge(int delAge) {
			this.DelAge = delAge;
		}
		public String getDelAddress() {
			return DelAddress;
		}
		public void setDelAddress(String delAddress) {
			this.DelAddress = delAddress;
		}
		public String getDelArea() {
			return DelArea;
		}
		public void setDelArea(String delArea) {
			this.DelArea = delArea;
		}
		public String getDelUsername() {
			return DelUsername;
		}
		public void setDelUsername(String delUsername) {
			this.DelUsername = delUsername;
		}
		public String getDelPassword() {
			return DelPassword;
		}
		public void setDelPassword(String delPassword) {
			this.DelPassword = delPassword;
		}
		
		@Override
		public String toString() {
			return "CustomerData [DelId=" + DelId + ",DelName=" + DelName + ", DelGender=" + DelGender + ", DelAge=" + DelAge
					+ ", DelAddress=" + DelAddress + ", DelArea=" + DelArea + ", DelUsername=" + DelUsername
					+ ", DelPassword=" + DelPassword + "]";
		}
		public DelPerData() {
			super();
		}

	    
}
