package Data;

public class CustomerData {
		private int Id;
	    private String name;
	    private int custAge;
	    private Double amountDue;
	    private char gender ;
	    private int availibilty;
	    private String custAddress;
	    private String area;
		public CustomerData(int id, String name2, int custAge, double amountDue, char gender,
				int availibilty, String custAddress, String area) {
			super();
			this.Id=id;
			this.name = name2;
			this.custAge = custAge;
			this.amountDue = amountDue;
			this.gender = gender;
			this.availibilty = availibilty;
			this.custAddress = custAddress;
			this.area = area;
		}
		public String getCustname() {
			return name;
		}
		public void setCustname(String name) {
			this.name = name;
		}
		public int getCustAge() {
			return custAge;
		}
		public void setCustAge(int custAge) {
			this.custAge = custAge;
		}
		public Double getAmountDue() {
			return amountDue;
		}
		public void setAmountDue(Double amountDue) {
			this.amountDue = amountDue;
		}
		public char getGender() {
			return gender;
		}
		public void setGender(char gender) {
			this.gender = gender;
		}
		public int getAvailibilty() {
			return availibilty;
		}
		public void setAvailibilty(int availibilty) {
			this.availibilty = availibilty;
		}
		public String getCustAddress() {
			return custAddress;
		}
		public void setCustAddress(String custAddress) {
			this.custAddress = custAddress;
		}
		public String getArea() {
			return area;
		}
		public void setArea(String area) {
			this.area = area;
		}
		@Override
		public String toString() {
			return "CustomerData [ custLastName=" + name + ", custAge="
					+ custAge + ", amountDue=" + amountDue + ", gender=" + gender + ", availibilty=" + availibilty
					+ ", custAddress=" + custAddress + ", area=" + area + "]";
		}
		public int getId() {
			return Id;
		}
		public void setId(int id) {
			Id = id;
		}
	    
}
