package interfaces;
import java.sql.SQLException;

import Data.DelPerData;
public interface DelPerDao {
	String adddelper(DelPerData DelPerData);
	
	//DelPerData findById(int id);
	DelPerData findById(int id);

	//int update(int id);

	String update(DelPerData customer1) throws Exception;

	String detele(int id) throws SQLException;
	

	//int update(int id);
}
