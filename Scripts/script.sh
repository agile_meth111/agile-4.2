rm -rf Newspaper\ Delivery\ System/bin
mkdir Newspaper\ Delivery\ System/bin
find Newspaper\ Delivery\ System/src -type f -name '*.java' -exec javac -d Newspaper\ Delivery\ System/bin -cp Newspaper\ Delivery\ System/lib/junit-3.8.1.jar {} +
if [ $? -ne 0 ]; then
	exit 1;
fi

cd Newspaper\ Delivery\ System/bin
find ./ -type f -name '*.class' -exec jar cfe NewspaperDeliverySystem.jar Create_Cust {} +
if [ $? -ne 0 ]; then
  echo "Jar Not creating";
else
  echo "Jar creating";
fi 


cd Newspaper\ Delivery\ System/bin
find ./ -type f -printf "%f\n"